du findest hierin eine seminararbeit.tex. Das ist die Datei die du kompilierst.

Im Ordner tex_support findest du verschiedene sinnvoll benannte Dateien. Den Ordnernamen darfst du *nicht* ändern, sonst gehen die \input{..} Befehle kaputt. 

Die Antiplagiatserklärung und bibliography sind selbsterklärend. In der bibliography.tex ist ein Beispieleintrag. Bei der Erklärung willst du evtl. mal schauen, ob es ne neuere Version gibt.

In der commands.tex fügst du, wenn gewünscht, eigene Abkürzungen oÄ hinzu. Wenn du eigene Packages hinzufügen musst, trag sie in der packages.tex ein. Wenn nicht, lass einfach so.

In der main.tex findet der eigentliche Inhalt statt. Sie bindet bspw. die inhalt.tex und fazit.tex ein. (Diese kannst du auch gefahrlos umbenennen.) Gegebenenfalls fügst du für weitere Kapitel einfach weitere .tex-Dateien hinzu und bindest sie mit \input{tex_support/Dateiname} in der main.tex ein.

In der titel.tex trägst du den Titel, Untertitel, deinen Namen, sowie Seminar- und persönliche Daten ein. Die werden dann automatisch auf der Titelseite verteilt, bzw in die Kopfzeile jeder Seite, und noch ein paar Dinge die ich wahrscheinlich vergesse. Benutze unbedingt  die dort schon eingetragenen Überschreibkommandos, sonst kann viel kaputtgehen.


Und schließlich der Ordner tex_support/magie. Den lässt du am besten in Ruhe, der macht die eben erwähnte Verteilung über die Seiten hinweg.
